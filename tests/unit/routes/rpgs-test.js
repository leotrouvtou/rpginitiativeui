import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | rpgs', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:rpgs');
    assert.ok(route);
  });
});
