import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | rpg/character', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:rpg/character');
    assert.ok(route);
  });
});
