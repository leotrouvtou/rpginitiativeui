import Component from '@ember/component';

export default Component.extend({
  store: Ember.inject.service(),
  actions: {
    deleteRpg(id) {
      let store = this.get('store');
      store.findRecord('rpg', id).then(function(post) {
        post.destroyRecord();
    })
  }
}
});
