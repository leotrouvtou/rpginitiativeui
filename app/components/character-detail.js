import Component from '@ember/component';
import { get, set } from '@ember/object';
import ENV from 'rpg-initiative-ui/config/environment';
export default Component.extend({
  actions: {
    incrementProperty(name, value) {
      let { character } = this;
      let total=get(character, name) + value
      let url=ENV.api.host+'/character/'+character.id+'/'+name+'/'+total
      Ember.$.ajax({
        type: 'PUT',
        url: url,
      }).done(function () {
        console.log('SUCCESS');
        set(character, name, total);
      }).fail(function (msg) {
        console.log('FAIL');
      })
    },
    toggleWait() {
      let { character } = this;
      set(character, 'wait', !character.wait);
    }
  }
});
