import Route from '@ember/routing/route';

export default Route.extend({
    async model({ rpg_id: rpgId }) {
      let rpg = await this.store.find('rpg', rpgId)
      return this.store.createRecord('character', { rpg });
    }
});
