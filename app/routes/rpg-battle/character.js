import Route from '@ember/routing/route';

export default Route.extend({
  model({ character_id:id  }) {
    return this.store.find('character', id)
  }
});
