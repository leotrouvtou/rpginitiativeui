import Route from '@ember/routing/route';

export default Route.extend({
  model({ id }) {
    return this.store.findRecord('rpg', id);
    //return this.store.query('character', {'rpgid' : id});

    // return [
    //   {
    //   "id": 3,
    //   "name": "VE",
    //   "power": 60,
    //   "actualPower": 40,
    //   "action": 5,
    //   "actualAction": 3,
    //   "init": 17
    // },
    //   {
    //     "id": 12,
    //     "name": "PJ",
    //     "power": 36,
    //     "actualPower": 30,
    //     "action": 4,
    //     "actualAction": 2,
    //     "init": 11
    //   },{
    //     "id": 122,
    //     "name": "BAT",
    //     "power": 36,
    //     "actualPower": 30,
    //     "action": 4,
    //     "actualAction": 1,
    //     "init": 9
    //   },{
    //     "id": 199,
    //     "name": "PR",
    //     "power": 64,
    //     "actualPower": 51,
    //     "action": 6,
    //     "actualAction": 2,
    //     "init": 7
    //   },{
    //     "id": 19,
    //     "name": "FLV",
    //     "power": 40,
    //     "actualPower": 25,
    //     "action": 6,
    //     "actualAction": 2,
    //     "init": 7
    //   },{
    //     "id": 16,
    //     "name": "BC",
    //     "power": 24,
    //     "actualPower": 23,
    //     "action": 6,
    //     "actualAction": 2,
    //     "init": 6
    //   },{
    //   "id": 14,
    //   "name": "MN",
    //   "power": 32,
    //   "actualPower": 6,
    //   "action": 6,
    //   "actualAction": 2,
    //   "init": 5
    // },{
    //   "id": 189,
    //   "name": "SLC",
    //   "power": 64,
    //   "actualPower": 23,
    //   "action": 6,
    //   "actualAction": 2,
    //   "init": 4
    // }]

  }
});
