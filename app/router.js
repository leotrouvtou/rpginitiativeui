import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('rpg', { path: '/rpg/:id'}, function() {
    this.route('character', { path: '/character/:character_id'});
  });
  this.route('rpgs', { path: '/'});
  this.route('character', { path: '/character/:id'});
  this.route('addcharacter', { path: '/rpg/:rpg_id/addcharacter'});
  this.route('addrpg');
  this.route('rpg-battle', { path: '/rpg-battle/:id'}, function() {
    this.route('character', { path: '/character/:character_id'});
  });
});

export default Router;
