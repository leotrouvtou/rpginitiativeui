import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
      deleteRpg(id) {
        this.store.findRecord('rpg', id).then(function(post) {
          post.destroyRecord();
      })
  }
}

});
