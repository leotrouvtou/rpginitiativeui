import Controller from '@ember/controller';
import groupBy from 'ember-group-by';
import sortBy from 'ember-computed-sortby';

export default Controller.extend({
  characterByInit: sortBy('model.characters', 'initiative:desc'),
  actions: {
    deleteCharacter(id) {
    this.store.findRecord('character', id).then(function(post) {
      post.destroyRecord();
    })
  }
}

});
