import Controller from '@ember/controller';
import groupBy from 'ember-group-by';
import sortBy from 'ember-computed-sortby';

export default Controller.extend({
  characterByInit: groupBy('model.characters', 'initiative')
});
