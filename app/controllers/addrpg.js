import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    addrpg() {
      let rpg = this.store.createRecord('rpg', {
        name: this.get('name'),
        powername: this.get('powername'),
        maxinit: this.get('maxinit')
      });
      rpg.save().then(() =>{
        this.transitionToRoute('rpgs');
        }).catch(function(reason) {
          debugger
        });
    }
  }
});
