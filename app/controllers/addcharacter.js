import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    addcharacter() {
      let character = this.model;
      character.setProperties({
        name: this.name,
        trigram: this.trigram,
        initiative: this.initiative,
        power: this.power,
        action: this.action,
        pv:this.pv,
        friend: this.friend,
      });
      character.save().then(() => {
        this.transitionToRoute('rpg', character.rpg);
      }).catch(function(reason) {
        debugger
      });
    }
  }
});
