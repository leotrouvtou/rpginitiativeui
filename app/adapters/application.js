import DS from 'ember-data';
import config from 'rpg-initiative-ui/config/environment';
import { reads } from '@ember/object/computed';
import { camelize } from '@ember/string';
import ENV from 'rpg-initiative-ui/config/environment';
const {
  RESTAdapter
} = DS

export default RESTAdapter.extend({
  namespace: ENV.api.namespace,
  host: ENV.api.host,
  pathForType(type) {
    return camelize(type);
  }
});
