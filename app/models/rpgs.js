import DS from 'ember-data';

const {
  attr,
  Model
} = DS;

export default DS.Model.extend({
  rpg: DS.hasMany('rpg', { async: true })
});
