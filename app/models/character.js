import DS from 'ember-data';

const {
  attr,
  Model
} = DS;

export default Model.extend({
  name: attr('string'),
  trigram: attr('string'),
  initiative: attr('number'),
  pv: attr('number'),
  actualpv: attr('number'),
  power: attr('number'),
  actualpower: attr('number'),
  action: attr('number'),
  actualaction: attr('number'),
  friend: attr('boolean'),
  wait: attr('boolean'),
  rpg: DS.belongsTo('rpg')
});
