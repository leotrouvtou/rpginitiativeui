import DS from 'ember-data';

const {
  attr,
  Model
} = DS;

export default DS.Model.extend({
  name: attr('string'),
  powername: attr('string'),
  maxinit: attr('string'),
  characters: DS.hasMany('character')
});
